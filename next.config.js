/* eslint disable */
module.exports = {
  webpack(config, { dev }) {
    config.module.rules.push(
      {
        test: /\.css$/,
        use: [
          'babel-loader',
          'styled-modules/loader',
          'css-loader?modules',
        ],
      },
    );
    return config;
  }
};