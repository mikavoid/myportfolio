import React, { Component } from 'react'
import Game from './components/Game'

export default class Home extends Component {
  render () {
    return (
      <div>
        <h1>Home</h1>
        <Game
          width="20"
          height="10"
        />
      </div>
    )
  }
}
