import React from 'react'
import Cell from './Cell/Cell'
import classes from './Grid.css'
import { STATUS } from '../../../contants'

const Grid = (props) => {
  const { w, h, snake, goal, status } = props

  const gridStyle = status === STATUS.GAME_STARTED ? {
    display: 'grid',
    gridTemplateColumns: `repeat(${w}, 1fr)`,
    gridTemplateRows: `repeat(${h}, 1fr)`
  } : {
    display: 'flex',
    width: `100%`
  }
  let content = null

  if (status === STATUS.GAME_STARTED) {
    const cells = []
    for (let line = 0; line < h; line++) {
      for (let col = 0; col < w; col++) {
        const isSnaked = snake.some(snakeCell => {
          return (snakeCell.x === col) && (snakeCell.y === line)
        })
        const isGoaled = col === goal.x && line === goal.y
        cells.push(<Cell
          key={col + ':' + line}
          x={col}
          y={line}
          snaked={!!isSnaked}
          goaled={!!isGoaled}
        />)
      }
      content = cells
    }
  } else {
    if (status === STATUS.GAME_UNSTARTED) {
      content = (
        <div className={classes.Unstarted}>
          Canal VOD
        </div>
      )
    } else if (status === STATUS.GAME_OVER) {
      content = (
        <div className={classes.Unstarted}>
          Yes !
        </div>
      )
    }
  }

  return (
    <div
      style={gridStyle}
      className={classes.Grid}
    >
      { content }
    </div>
  )
}

export default Grid
