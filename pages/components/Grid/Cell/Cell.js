import React from 'react'
import classes from './Cell.css'

const Cell = ({ x, y, snaked, goaled }) => {
  const cellClasses = [classes.Cell]
  if (snaked) cellClasses.push(classes.Snaked)
  if (goaled) cellClasses.push(classes.Goaled)
  return (
    <span
      className={cellClasses.join(' ')}
    >
      {/* [{x}:{y}] */}
      { goaled ? 'X' : ''}
    </span>
  )
}

export default Cell
