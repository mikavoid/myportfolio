import React, { Component } from 'react'
import Grid from './Grid/Grid'
import classes from './Game.css'
import { DIRECTIONS, STATUS } from '../../contants'

class Game extends Component {
  state = {
    leftToEat: 10,
    snake: [],
    direction: DIRECTIONS.RIGHT,
    speed: 400,
    currentGoal: { x: 0, y: 0 },
    status: STATUS.GAME_UNSTARTED
  }

  generateGoal = () => {
    const { width, height } = this.props
    let x = Math.floor(Math.random() * width)
    let y = Math.floor(Math.random() * height)

    while (this.state.snake.some(s => s.x === x && s.y === y)) {
      x = Math.floor(Math.random() * width)
      y = Math.floor(Math.random() * height)
    }

    this.setState({
      currentGoal: { x, y }
    })
  }

  initGame = () => {
    this.setState({
      leftToEat: 10,
      snake: [],
      direction: DIRECTIONS.RIGHT,
      speed: 400,
      currentGoal: { x: 0, y: 0 },
      status: STATUS.GAME_UNSTARTED
    })
  }

  startGame = (snakeSize = 4) => {
    if (this.state.status === STATUS.GAME_STARTED) return
    this.initGame()
    const { width, height } = this.props
    const snake = []
    let direction = DIRECTIONS.DOWN
    const xPos = Math.round((width / 2) - (snakeSize / 2))
    const yPos = Math.round((height / 2))

    // Init Snake
    for (let i = 0; i < snakeSize; i++) {
      snake.push({ x: (i + xPos), y: yPos })
    }

    // Init direction
    if (width > height) direction = DIRECTIONS.RIGHT

    // Init first goal
    this.generateGoal()
    this.setState({
      snake,
      direction,
      status: STATUS.GAME_STARTED
    })
    this.interval = setInterval(this.animateSnake, this.state.speed)
  }

  handleCollision = (head) => {
    const goal = this.state.currentGoal
    const { leftToEat, snake, speed } = this.state
    if (head.x === goal.x && head.y === goal.y) {
      // if eating goal
      this.generateGoal()
      this.setState({ leftToEat: leftToEat - 1, speed: speed - 40 }, this.handleGameOver())
      return true
    } else if (snake.some(s => s.x === head.x && s.y === head.y)) {
      // collision with myself
      this.setState({ leftToEat: 0 }, this.handleGameOver())
    }
    return false
  }

  handleGameOver = () => {
    const { leftToEat } = this.state
    if (leftToEat <= 0) {
      this.setState({ status: STATUS.GAME_OVER })
      clearInterval(this.interval)
    }
  }

  animateSnake = () => {
    console.log('###')
    if (!this.state.snake.length) return
    const snake = [...this.state.snake]
    const { width, height } = this.props
    const { direction } = this.state

    // remove last elem
    const head = { ...snake[snake.length - 1] } // get the last elem
    if (direction === DIRECTIONS.RIGHT || direction === DIRECTIONS.LEFT) {
      head.x = direction === DIRECTIONS.RIGHT ? head.x + 1 : head.x - 1
      head.x = head.x < 0 ? width - 1 : head.x % width
    } else {
      head.y = direction === DIRECTIONS.UP ? head.y - 1 : head.y + 1
      head.y = head.y < 0 ? height - 1 : head.y % height
    }
    if (!this.handleCollision(head)) snake.shift()
    clearInterval(this.interval)
    if (this.state.status === STATUS.GAME_STARTED) {
      this.interval = setInterval(this.animateSnake, this.state.speed)
    }
    snake.push(head)
    this.setState({
      snake,
      lockedControls: false
    })
  }

  onKeyDown = ({ key }) => {
    const { direction, lockedControls } = this.state
    if (lockedControls) return
    let newDir = DIRECTIONS.RIGHT
    if (key === 'ArrowDown' || key === 'ArrowUp') {
      if (direction === DIRECTIONS.DOWN || direction === DIRECTIONS.UP) return
      newDir = key === 'ArrowDown' ? DIRECTIONS.DOWN : DIRECTIONS.UP
      this.setState({ direction: newDir, lockedControls: true })
    }
    if (key === 'ArrowLeft' || key === 'ArrowRight') {
      if (direction === DIRECTIONS.RIGHT || direction === DIRECTIONS.LEFT) return
      newDir = key === 'ArrowLeft' ? DIRECTIONS.LEFT : DIRECTIONS.RIGHT
      this.setState({ direction: newDir, lockedControls: true })
    }
  }

  render () {
    const {
      height = 10,
      width = 20
    } = this.props
    return (
      <div tabIndex={0} onClick={() => this.startGame(3) } onKeyDown={this.onKeyDown} className={classes.Game}>
        <Grid
          status={this.state.status}
          w={width}
          h={height}
          snake={this.state.snake}
          goal={this.state.currentGoal}
        />
      </div>
    )
  }
}

export default Game
