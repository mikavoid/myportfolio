export const DIRECTIONS = {
  RIGHT: 'right',
  LEFT: 'left',
  UP: 'up',
  DOWN: 'down'
}

export const STATUS = {
  GAME_STARTED: 1,
  GAME_UNSTARTED: 2,
  GAME_OVER: 3,
  GAME_WILLSTART: 4
}
